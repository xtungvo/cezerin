// config used by dashboard client side only
module.exports = {
	// dashboard UI language
	language: 'en',
	apiBaseUrl: 'http://45.122.222.184:3001/api/v1',
	apiWebSocketUrl: 'ws://45.122.222.184:3001',
	developerMode: true
};
